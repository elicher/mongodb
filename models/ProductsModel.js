const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const productSchema = new Schema({

    category_id: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'category'
	},
	name: {
		type: String,
		unique: true
	},
	price:Number,
	color:String,
	description:String	
	})

module.exports =  mongoose.model('products', productSchema);
