const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    categoryRoute = require('./routes/CategoryRoute'),
    productRoute = require('./routes/ProductRoute'),
    bodyParser = require('body-parser');
    const { response } = require('express')

// =================== initial settings ===================
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect('mongodb+srv://elicher:123456789HELLO@testapp.qtev7.mongodb.net/testapp?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB!')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// end of connecting to mongo and checking if DB is running

// routes
app.use('/category', categoryRoute);
app.use('/product', productRoute);

// Set the server to listen on port 3000
app.listen(3014, () => console.log(`listening on port 3014`))

///

