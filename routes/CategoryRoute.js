const express     = require('express'),
    router        = express.Router(),
    controller    = require('../controllers/CategoryController');

//  == This route will give us back all categories: ==  //

router.get('/categories', controller.findAll);

// //  == This route will give us back one category's all products, it will be that with the id we are providing: ==  //

router.get('/:category_id', controller.find);

// // //  == This route allow us to add an extra category: ==  //

router.post('/add', controller.insert);

// //  == This route allow us to delete one category, it will be that with the id we are providing: ==  //

router.post('/delete', controller.delete);

// //  == This route allow us to update one category, it will be that with the id we are providing ==  //

router.post('/update', controller.update);



module.exports = router;