const { db } = require('../models/ProductsModel.js');
const Products = require('../models/ProductsModel.js');
const express     = require('express'),
router = express.Router();

class ProductController {

/// works ///

    async findAll(req, res){
        try{
            const prods = await Products.find();
            res.send(prods);
        }
        catch(e){
            res.send({e})
        }
    }

/// works ///

async findOne(req ,res){
        let { product } = req.params;
        try{
            const prod = await Products.find({"name":product});
            res.send(prod);
        }
        catch(e){
            res.send({e})
        }}

/// works ///

    async insert (req, res) {
        let { name,price,description,color,category_id } = req.body;
        try{
            const done = await Products.create({category_id,name,price,color,description});
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }


/// works ///

    async delete (req, res){
        console.log('delete!!!')
        let { name } = req.body;
        try{
            const removed = await Products.deleteOne({"name":name});
            res.send(removed);
        }
        catch(error){
            res.send({error});
        };
    }


/// works ///

    async update (req, res){
        let { name, newName, newPrice, newDescription, newColor, newCategory_id } = req.body;
        try{
            const updated = await Products.updateOne(
                { name },{ name:newName, category_id: newCategory_id, color: newColor, price: newPrice, description: newDescription}
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


};

module.exports = new ProductController();