const Cats = require('../models/CategoryModel.js');
const Products = require('../models/ProductsModel.js');

// works //

class CategoryController {
    async findAll(req, res){
        try{
            const cats = await Cats.find({});
            res.send(cats);
        }
        catch(e){
            res.send({e})
        }
    }

// works ///

    async find(req ,res){
        let { category_id } = req.params;
        try{
            const cat = await Products.find({category_id:category_id});
            res.send(cat);
        }
        catch(e){
            res.send("Category already exists")
        }  }

// works ///

    async insert (req, res) {
        let { category } = req.body;
        try{
            const done = await Cats.create({category});
            res.send(done)
        }
        catch(e){
            res.send({e})
        }
    }
 
// works ///
    
    async delete (req, res){
        let { category } = req.body;
        try{
            const removed = await Cats.deleteOne({ category });
            res.send({removed});
        }
        catch(error){
            res.send({error});
        };
    }

/// works ///
    
    async update (req, res){
        let { category, newCategory } = req.body;
        try{
            const updated = await Cats.updateOne(
                { category },{ category:newCategory }
             );
            res.send({updated});
        }
        catch(error){
            res.send({error});
        };
    }


};
module.exports = new CategoryController();